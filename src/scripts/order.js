
var bookURL = [];
bookURL = location.search.substring(1).split('&'); //взяли из адресной строки id книги
var currencyChar; //код валюты
var bookId = bookURL[0]; //ID книги
var currencyId = bookURL[1]; //ID валюты
var bookCost; //стоимость книги
var bookCostDelivery;// общая стоимость книги, включая доставку
var orderDelivery; //выбранный способ доставки
var orderPayment; //выбранный способ оплаты
var bookTitle;

bookTitle = document.getElementById('book_title');//тэг, в котором будет название книги
bookTitle.className = 'book_name';
var bookCover = document.getElementById('book_cover');  //тэг, в котором будет обложка книги
var coverImg; //картинка обложки
var linkCover; // ссылка обложки
var bookName; // название книги
var link;
var addressError;

var request = new XMLHttpRequest();//запрос для поиска книги
request.open('GET', 'https://netology-fbb-store-api.herokuapp.com/book/'+ bookId);
request.send();

request.addEventListener('load', function (event) {
    if (request.status === 200) {
        var book = JSON.parse(request.responseText);

        bookName = document.createElement('div');
        bookTitle.appendChild(bookName);
        bookName.className = 'book_name';
        bookName.innerHTML = 'Оформить заказ на книгу ';

        link = document.createElement('a');
        link.className = 'link';
        bookTitle.appendChild(link);
        link.href = "book_card.html?" + bookId;
        link.innerHTML = book.title;

        linkCover = document.createElement('a');
        linkCover.className = 'link';

        coverImg = document.createElement('img');
        coverImg.className = 'coverImg';

        if(document.documentElement.clientWidth < 800) {  //вариант размещения обложки книги в зависимости от ширины окна
            bookTitle.appendChild(linkCover);
            linkCover.appendChild(coverImg);
            linkCover.href = "index.html";
            coverImg.src = book.cover.small;
        }
        else{
            bookCover.appendChild(linkCover);
            linkCover.appendChild(coverImg);
            linkCover.href = "index.html";
            coverImg.src = book.cover.small;
        }

        bookCost = book.price;
    }
})

//cпособы доставки
var requestDelivery = new XMLHttpRequest();
var deliveryLabel = document.querySelector('.delivery');
var method = [];
var label = [];
var addressHeader;
var address;
var delivery;
var paymentLabel;
var methodPay = [];
var labelPay = [];
var deliveryPayment;
var available = [];

requestDelivery.open('GET', 'https://netology-fbb-store-api.herokuapp.com/order/delivery');
requestDelivery.send();

requestDelivery.addEventListener('load', function (event) {
    if (requestDelivery.status === 200) {
        delivery = JSON.parse(requestDelivery.responseText);

        delivery.forEach(function(value, i){
            label [i] = document.createElement('p');
            deliveryLabel.appendChild(label[i]);
            method[i] = document.createElement('input');
            method[i].type = 'radio';
            method[i].name = 'deliv';
            label[i].className = 'deliv_options'
            method[i].value = value.id;
            label[i].appendChild(method[i]);
            label[i].innerHTML += value.name;
            document.getElementsByName('deliv')[0].checked = true; // по умолчанию выбран первый вариант доставки

            bookCostDelivery = bookCost + delivery[0].price; //добавлена стоимость доставки по первому варианту
        })
        orderDelivery = delivery[0].id;
        createPayment('delivery-01');

        function createPayment(valueDel) {
           // для первого типа доставки, который checked по умолчанию, формируем способы оплаты
           paymentLabel = document.querySelector('.payment');
           var requestDeliveryPay = new XMLHttpRequest();
           requestDeliveryPay.open('GET', 'https://netology-fbb-store-api.herokuapp.com/order/payment');
           requestDeliveryPay.send();

           requestDeliveryPay.addEventListener('load', function (event) {
              if (requestDeliveryPay.status === 200) {
                  deliveryPayment = JSON.parse(requestDeliveryPay.responseText);

                  deliveryPayment.forEach(function (value, i) {
                      labelPay [i] = document.createElement('p');
                      labelPay[i].className = 'pay_options';

                      methodPay[i] = document.createElement('input');
                      methodPay[i].type = 'radio';
                      methodPay[i].name = 'payM';
                      methodPay[i].value = value.id;

                      methodPay[i].disabled = false;

                      paymentLabel.appendChild(labelPay[i]);
                      labelPay[i].appendChild(methodPay[i]);

                      available = value.availableFor;
                      if (checkRadio(available, valueDel)) {
                          methodPay[i].disabled = true;
                          labelPay[i].style.color = '#c2c2c2';
                      }

                      labelPay[i].innerHTML += value.title;
                      //document.getElementsByName('payM')[0].checked = true;
                  })
                  orderPayment = deliveryPayment[0].id;
              }
           })
        }

       function checkRadio (arrayPay, valueDel){ //функция проверки доступности радиокнопок оплаты для данного вида доставки
          function DeliveryPos(value) {
             return (value == valueDel);
          }
          if (!arrayPay.some(DeliveryPos)) {
             return true;
          }
       }

       function needAddress (value) {  // функция проверки, нужно ли вводить адрес?
          if (value.needAdress == true) {
             address.disabled = false;
          }
          else {
             address.disabled = true;
          }
       }

        // показывать поле ввода адреса
        addressHeader = document.createElement('p');
        addressHeader.className = 'order_field';
        address = document.createElement('textarea');
        address.name = 'addressDeliv';
        address.className = 'address';
        addressHeader.innerHTML = "Адрес доставки:";
        deliveryLabel.appendChild(addressHeader);
        deliveryLabel.appendChild(address);
        addressError = document.createElement('p');
        addressError.className = 'error';
        addressError.id = 'err_address';
        deliveryLabel.appendChild(addressError);
        needAddress(delivery[0]);

        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://netology-fbb-store-api.herokuapp.com/currency/');
        xhr.send();
        xhr.addEventListener('load', function (event) {
           if (xhr.status === 200) {
              var currency = JSON.parse(xhr.responseText);
              var currencyBook = currency.find(function(value){
                 return (value.ID == currencyId);
              });
              currencyChar = currencyBook.CharCode;
              var cost = document.querySelector('.book_cost');
              cost.innerHTML = 'Итого к оплате: ' + bookCostDelivery +' '+currencyChar;
           }
        })

        var delivArray = document.getElementsByName('deliv');
        var paymentArray = document.getElementsByName('payMethod');

        // функция изменений интерфейса от способа доставки
        function checkDelivery(event) {
           orderDelivery = event.target.value; //ID доставки
           var deliveryCheck = delivery.find(function(value){ //данные для этого способа доставки
              return (value.id == orderDelivery);
           });

           // показывать поле ввода адреса
           needAddress(deliveryCheck);

           //показывать способы оплаты
            var arrayP = []; //массив способов оплаты из документа
            arrayP = document.getElementsByName('payM');
            var arrayL = [];
            arrayL = document.getElementsByClassName('pay_options');
            console.log (arrayL);
            deliveryPayment.forEach(function (value, i) {
               arrayP[i].disabled = false;
               arrayP[i].checked = false;
               arrayL[i].style.color = '#4c4c4c';
               available = value.availableFor;
               if (checkRadio(available, orderDelivery)) {
                   arrayP[i].disabled = true;
                   arrayL[i].style.color = '#c2c2c2';
               }
            })

           function checkPayment(event) { //функция обработки выбранного способа оплаты
              orderPayment = event.target.value;
                 var paymentCheck = deliveryPayment.find(function(value){
                   return (value.id == event.target.value);
                 });
           }

           // вызов функции checkPayment обработки выбранного способа оплаты
           for (i = 0; i < paymentArray.length; i++) {
              paymentArray[i].addEventListener('change', checkPayment);
           }

           // определение общей стоимости книги
           bookCostDelivery = bookCost + deliveryCheck.price;
           cost = document.querySelector('.book_cost');
           cost.innerHTML = 'Итого к оплате: ' + bookCostDelivery +' '+currencyChar;
        }

       //вызов функции checkDelivery (event) обработки выбранного способа доставки
       for (i = 0; i < delivArray.length; i++) {
          delivArray[i].addEventListener('change', checkDelivery);
       }
    }

})

//отправка данных
var orderForm = document.forms.order;

orderForm.addEventListener('submit', function(event) {
    event.preventDefault();
    submitForm();
});

function submitForm() {
    var request_order = new XMLHttpRequest();
    request_order.open('POST', 'https://netology-fbb-store-api.herokuapp.com/order');
    request_order.setRequestHeader('Content-Type', 'application/json');

    var orderName = document.querySelector('.name').value;
    var orderPhone = document.querySelector('.phone').value;
    var orderEmail = document.querySelector('.email').value;
    var orderComment = document.querySelector('.comment_order').value;
    var orderAddress = document.querySelector('.address').value;

    //проверка введенных данных
    if ( orderName == '') {
        document.getElementById('err_fio').innerHTML = 'Как Вас зовут?';
    }
    else{
        document.getElementById('err_fio').innerHTML = '';
    };
    if ( orderPhone == '') {
        document.getElementById('err_phone').innerHTML = 'Нам бы телефончик Ваш...';
    }
    else{
        document.getElementById('err_phone').innerHTML = '';
    };
    if ( orderEmail == '') {
        document.getElementById('err_email').innerHTML='Почту не указали. Куда же нам прислать данные заказа?';
    }
    else{
        document.getElementById('err_email').innerHTML='';
    };
    if ( orderComment == '') {
        document.getElementById('err_comment').innerHTML='Ну напишите нам еще немного о себе';
    }
    else{
        document.getElementById('err_comment').innerHTML='';
    };
    if ( orderAddress == '') {
        document.getElementById('err_address').innerHTML='Ну, а как же без адреса?';
    }
    else{
        document.getElementById('err_address').innerHTML='';
    };

    var body = {
        manager: 'okorepanova28@mail.ru',
        book: bookId,
        name: orderName,
        phone: orderPhone,
        email: orderEmail,
        comment: orderComment,
        delivery: {
            id: orderDelivery,
            address: orderAddress
        },
        payment: {
            id: orderPayment,
            currency: currencyId
        }
    };

    request_order.send(JSON.stringify(body));
    var orderAnswer = document.createElement('h2');
    var inputLeft;
    var inputMiddle;
    var inputRight;
    request_order.addEventListener('load', function (event) {
        if (request_order.status === 200) {
            bookName.innerHTML = 'Заказ на книгу успешно оформлен. Спасибо, что спасли книгу от сжигания в печи.';
            link.innerHTML = '';

            inputLeft = document.querySelector('.order-1');
            inputLeft.style.display = 'none';

            inputMiddle = document.querySelector('.order-2');
            inputMiddle.style.display = 'none';

            inputRight = document.querySelector('.order-3');
            inputRight.style.display = 'none';
        }
           else {
              link.innerHTML = '';
              bookName.innerHTML = 'Сожалеем, возможно где-то закралась ошибочка в данных. Поищем?';
           };
    })
}















