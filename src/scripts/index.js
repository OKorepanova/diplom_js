//вывод книг на стартовой странице

var request = new XMLHttpRequest();
request.open('GET', 'https://netology-fbb-store-api.herokuapp.com/book');
request.addEventListener('load', function (event) {
if (request.status === 200) {
  var books = JSON.parse(request.responseText);
  var cover = [];
  var cite = [];
  var book = [];
  var link = [];
  var bookId;
  var num = 0; // счетчик количества выведенных на экран книг
  var buttonAdd; // кнопка
  var BOOKS_ON_PAGE = 4; //количество книг на странице
  var books_page = document.getElementById('books_page');
  createButton();
  showBooks();

   //функция вывода порции книг
  function showBooks () {
      if (num > 0) {
          removeButton();
      }
      for (i = num; (i < num + BOOKS_ON_PAGE) && (i < books.length); i++) {
          book[i] = document.createElement('div');
          book[i].className = 'book';
          link[i] = document.createElement('a');
          link[i].className = 'link';
          cover[i] = document.createElement('img');
          cover[i].className = 'cover';
          cite[i] = document.createElement('div');
          cite[i].className = 'cite';
          book[i].appendChild(link[i]);
          link[i].appendChild(cover[i]);
          book[i].appendChild(cite[i]);
          books_page.appendChild(book[i]);
          bookId = books[i].id;
          link[i].href = "book_card.html?" + bookId;
          cover[i].src = books[i].cover.small;
          cite[i].innerHTML = books[i].info;
      }
      num += BOOKS_ON_PAGE; //добавляем в счетчик выведенные книги
      if (num < books.length) {
          showButton();
      }
  }

  function createButton() {
    buttonAdd = document.createElement('button');
    buttonAdd.className = 'buttonAdd';
    buttonAdd.innerHTML = 'Еще несколько книг';
  }

  function showButton() {
      books_page.appendChild(buttonAdd);
  }

  function removeButton() {
      books_page.removeChild(buttonAdd);
  }
  buttonAdd.onclick = function(){
    showBooks();
  }
}
})
request.send();

//поиск книг по автору и названию

var searchString = document.querySelector('.search_str');
var searchForm = document.forms.search;

searchForm.addEventListener('submit', function(event) {
    event.preventDefault();
    submitForm();
});

//вывод найденных книг
function submitForm(){
    var books_page = document.getElementById('books_page');
    var searchBooks = [];
    var request = new XMLHttpRequest();
    request.open('GET', 'https://netology-fbb-store-api.herokuapp.com/book');
    request.addEventListener('load', function (event) {
        if (request.status === 200) {
            var books = JSON.parse(request.responseText);
            console.log(books);
            var searchBooks = books.filter(function(value){
                console.log(value.title);
                console.log(searchString.value);
                if (value.title.toUpperCase().indexOf(searchString.value.toUpperCase())!=-1 || value.author.name.toUpperCase().indexOf(searchString.value.toUpperCase())!=-1) {
                    return value;
                }
            })

            var cover = [];
            var cite = [];
            var book = [];
            var link = [];
            var bookId;
            var num = 0; // счетчик количества выведенных на экран книг
            var buttonAdd; // кнопка
            var BOOKS_ON_PAGE = 4; //количество книг на странице

            createButton();
            clearBooks();
            showBooks();

            //функция вывода порции книг
            function showBooks () {
                if (num > 0) {
                    removeButton();
                }
                for (i = num; (i < num + BOOKS_ON_PAGE) && (i < searchBooks.length); i++) {
                    book[i] = document.createElement('div');
                    book[i].className = 'book';
                    link[i] = document.createElement('a');
                    link[i].className = 'link';
                    cover[i] = document.createElement('img');
                    cover[i].className = 'cover';
                    cite[i] = document.createElement('div');
                    cite[i].className = 'cite';
                    book[i].appendChild(link[i]);
                    link[i].appendChild(cover[i]);
                    book[i].appendChild(cite[i]);
                    books_page.appendChild(book[i]);
                    bookId = searchBooks[i].id;
                    link[i].href = "book_card.html?" + bookId;
                    cover[i].src = searchBooks[i].cover.small;
                    cite[i].innerHTML = searchBooks[i].info;
                }
                num += BOOKS_ON_PAGE; //добавляем в счетчик выведенные книги
                if (num < searchBooks.length) {
                    showButton();
                }
            }

            function createButton() {
                buttonAdd = document.createElement('button');
                buttonAdd.className = 'buttonAdd';
                buttonAdd.innerHTML = 'Еще несколько книг';
            }

            function showButton() {
                books_page.appendChild(buttonAdd);
            }

            function removeButton() {
                books_page.removeChild(buttonAdd);
            }
            buttonAdd.onclick = function(){
                showBooks();
            }

            function clearBooks() {
                var bender = document.querySelector('.bender');

                if (searchBooks.length >0) {
                    bender.style.marginTop = '4rem';
                    books_page.innerHTML = '';
                }
                else {
                    books_page.innerHTML = '';
                    var errorSearch = document.createElement('h2');
                    errorSearch.innerHTML = 'Такой книги у нас пока нет :(';
                    books_page.appendChild(errorSearch);
                    bender.style.marginTop = '18rem';
                }
            }
        }
    })
    request.send();
}