
var bookId = location.search.substring(1);
var currencyId;
console.log(bookId);
var request = new XMLHttpRequest();
request.open('GET', 'https://netology-fbb-store-api.herokuapp.com/book/'+ bookId);
request.send();

request.addEventListener('load', function (event) {
    if (request.status === 200) {
        var book_card = document.getElementById('book_card');
        var coverImg; //картинка обложки
        var link; // ссылка обложки
        var descriptionBook; //описание книги
        var cite = []; //цитаты
        var citePic; //автор цитаты
        var title= []; //особенности
        var titlePic; //картинки к ним
        var buttonBuy; //кнопка КУпить
        var eye;
        var eyeTracker;

        var book = JSON.parse(request.responseText);

        // создаем три родительских блока  для книги, rewiews и features
        var bookDiv = document.createElement('div');
        var reviewsDiv = document.createElement('div');
        var featuresDiv = document.createElement('div');
        book_card.appendChild(bookDiv);
        book_card.appendChild(reviewsDiv);
        book_card.appendChild(featuresDiv);
        bookDiv.className = 'book_div';
        reviewsDiv.className = 'reviews_div';
        featuresDiv.className = 'features_div';

        //создаем для них внутренние элементы

        book.reviews.forEach(function(value, i){
            citePic = document.createElement('img');
            reviewsDiv.appendChild(citePic);
            citePic.src = value.author.pic;
            cite = document.createElement('div');
            reviewsDiv.appendChild(cite);
            cite.innerHTML = "&laquo;" + value.cite + "&raquo;";
            citePic.className = 'citePic';
            cite.className = 'cite';
        })

        link = document.createElement('a');
        link.className = 'link';
        coverImg = document.createElement('img');
        coverImg.className = 'coverImg';
        eye = document.createElement('div');
        eye.className = 'eye';
        eyeTracker = document.createElement('div');
        eyeTracker.className = 'eye_tracker';
        descriptionBook = document.createElement('div');
        descriptionBook.className = 'descriptionBook';
        bookDiv.appendChild(link);
        link.appendChild(coverImg);
        bookDiv.appendChild(eye);
        eye.appendChild(eyeTracker);
        bookDiv.appendChild(descriptionBook);

        link.href = "index.html";
        if(document.documentElement.clientWidth > 800) {
            coverImg.src = book.cover.large;
        }
        else {
            coverImg.src = book.cover.small;
        }

        var resize = function(e){
            if(document.documentElement.clientWidth > 800) {
                coverImg.src = book.cover.large;
            }
            else {
                coverImg.src = book.cover.small;
            }
        };
        (function(){
            var time;
            window.onresize = function(e){
                if (time)
                    clearTimeout(time);
                time = setTimeout(function(){
                    resize(e);
                },1000);
            }
        })();

        descriptionBook.innerHTML = book.description;

        book.features.forEach(function(value, i){
            titlePic = document.createElement('img');
            featuresDiv.appendChild(titlePic);
            titlePic.src = value.pic;
            title = document.createElement('div');
            featuresDiv.appendChild(title);
            title.innerHTML = value.title;
            titlePic.className = 'titlePic';
            title.className = 'title';
        })
        var currencyID;
        currencyID = book.currency;

        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://netology-fbb-store-api.herokuapp.com/currency/');
        xhr.send();
        xhr.addEventListener('load', function (event) {
            if (xhr.status === 200) {
                var currency = JSON.parse(xhr.responseText);
                var currencyBook = currency.find(function(value){
                    return (value.ID == currencyID);
                });
                currencyId = currencyBook.ID;
                buttonBuy = document.createElement('button');
                buttonBuy.className = 'buttonBuy';
                buttonBuy.innerHTML = 'Купить \n за жалкие ' + book.price + ' ' + currencyBook.CharCode;
                buttonBuy.setAttribute('onclick', 'location.href = "order.html?" + bookId + "&"  +currencyId');
                book_card.appendChild(buttonBuy);
            }
        })
    }

    var bookObject = [];
    bookObject = document.getElementsByClassName('book_div');
    var bookObjectBox = bookObject[0].getBoundingClientRect();
    var eyeObject = [];
    eyeObject = document.getElementsByClassName('eye');
    var eyeObjectBox = eyeObject[0].getBoundingClientRect();

    eyeObject[0].style.left = bookObjectBox.width/2 - eyeObjectBox.width/2  + 'px';
    eyeObject[0].style.top = bookObjectBox.height/2 + eyeObjectBox.height*2.5 + 'px';

    var eyeTrackerObject = document.getElementsByClassName('eye_tracker');
    var eyeTrackerObjectBox = eyeTrackerObject[0].getBoundingClientRect();

    eyeTrackerObject[0].style.left = eyeObjectBox.width/2 - eyeTrackerObjectBox.width/2 + 'px';
    eyeTrackerObject[0].style.top = eyeObjectBox.height/2  - eyeTrackerObjectBox.height/2 + 'px';

    document.onclick = click;
    function click (event){
        var xCur = 0;
        var yCur = 0;
        xCur = event.clientX;
        yCur = event.clientY;

        bookObjectBox = bookObject[0].getBoundingClientRect();
        eyeObjectBox = eyeObject[0].getBoundingClientRect();
        eyeTrackerObjectBox = eyeTrackerObject[0].getBoundingClientRect();
        console.log ('bo', bookObjectBox, xCur, yCur);
        console.log('eo', eyeObjectBox, xCur, yCur);
        console.log('et', eyeTrackerObjectBox, xCur, yCur);
    }

    document.onmousemove=mousemove;

    function mousemove(event) {
        bookObjectBox = bookObject[0].getBoundingClientRect();
        eyeObjectBox = eyeObject[0].getBoundingClientRect();
        eyeTrackerObjectBox = eyeTrackerObject[0].getBoundingClientRect();
        var xCur = 0;
        var yCur = 0;
        xCur = event.clientX;
        yCur = event.clientY;
        var windowWidth = document.documentElement.clientWidth;
        var windowHeight = document.documentElement.clientHeight;

        var xCE = eyeObjectBox.left + eyeObjectBox.width/2 ;
        var yCE = eyeObjectBox.top + eyeObjectBox.height/2;
        var rE = eyeObjectBox.width/2-eyeTrackerObjectBox.width/2;

        if ((xCE > xCur) && (yCE > yCur)){
            var xA11 = 0;
            var yA11 = yCE - xCE * (yCur - yCE)/(xCur - xCE);
            var xA12 = xCE - yCE * (xCur - xCE) / (yCur - yCE);
            var yA12 = 0;
            if (yA11 >= 0 ){
              xA1 = 0;
              yA1 = yA11;
            }
            else {
                xA1 = xA12;
                yA1 = 0;
            }
            var rA = Math.sqrt((xA1 - xCE)*(xA1 - xCE) + (yA1 - yCE)*(yA1 - yCE));

            console.log('rE=', rE);
            var ZP = rE/rA;
            var XD = xCE + (xCur - xCE) * ZP;
            var YD = yCE + (yCur - yCE) * ZP;
        }
         if ((xCE < xCur) && (yCE > yCur)) {
            var xA21 = windowWidth;
            console.log('xA21', xA21);
            console.log('yCE', yCE, 'xCE', xCE, 'yCur', yCur, 'xCur', xCur);
            var yA21 = yCE + (xA21-xCE) * (yCur - yCE)/(xCur - xCE);
            var xA22 = xCE - yCE * (xCur - xCE) / (yCur - yCE);
            var yA22 = 0;
             console.log('xA21', xA21, 'yA21', yA21, 'xA22', xA22, 'yA22', yA22);
            if (yA21 >= 0 ){
                xA2 = xA21;
                yA2 = yA21;
            }
            else {
                xA2 = xA22;
                yA2 = 0;
            }
             console.log('xA21', xA21, 'yA21', yA21, 'xA22', xA22, 'yA22', yA22, 'xA2', xA2, 'yA2', yA2);
            var rA = Math.sqrt((xA2 - xCE)*(xA2 - xCE) + (yA2 - yCE)*(yA2 - yCE));
           // console.log('rE=', rE);
            var ZP = rE/rA;
            var XD = xCE + (xCur - xCE) * ZP;
            var YD = yCE + (yCur - yCE) * ZP;
        }
        if ((xCE > xCur) && (yCE < yCur)) {
            var xA31 = 0;
            var yA31 = yCE - xCE * (yCur - yCE)/(xCur - xCE);
            var yA32 = windowHeight;
            var xA32 = xCE + (yA32 - yCE) * (xCur - xCE) / (yCur - yCE);

            if (yA31 < windowHeight ){
                xA3 = xA31;
                yA3 = yA31;
            }
            else {
                xA3 = xA32;
                yA2 = yA32;
            }

            var rA = Math.sqrt((xA3 - xCE)*(xA3 - xCE) + (yA3 - yCE)*(yA3 - yCE));

            var ZP = rE/rA;
            var XD = xCE + (xCur - xCE) * ZP;
            var YD = yCE + (yCur - yCE) * ZP;
        }
        if ((xCE < xCur) && (yCE < yCur)) {
            var xA41 = windowWidth;
            var yA41 = yCE + (xA41- xCE) * (yCur - yCE)/(xCur - xCE);
            var yA42 = windowHeight;
            var xA42 = xCE + (yA42 - yCE) * (xCur - xCE) / (yCur - yCE);

            if (yA41 < windowHeight ){
                xA4 = xA41;
                yA4 = yA41;
            }
            else {
                xA4 = xA42;
                yA4 = yA42;
            }

            var rA = Math.sqrt((xA4 - xCE)*(xA4 - xCE) + (yA4 - yCE)*(yA4 - yCE));
            var ZP = rE/rA;
            var XD = xCE + (xCur - xCE) * ZP;
            var YD = yCE + (yCur - yCE) * ZP;
        }

        var shiftX = (XD - (xCE - rE));
        var shiftY = (YD - (yCE - rE));

        eyeTrackerObject[0].style.left = shiftX + 'px';
        eyeTrackerObject[0].style.top = shiftY + 'px';
    }


    var imagesEye = [
        "url('images/eye.png')",
        "url('images/veko_1.png')",
        "url('images/veko_2.png')",
        "url('images/veko_3.png')",
        "url('images/veko_4.png')",
        "url('images/veko_5.png')",
        "url('images/veko_4.png')",
        "url('images/veko_3.png')",
        "url('images/veko_2.png')",
        "url('images/veko_1.png')",
        "url('images/eye.png')"
    ];

    var i = 0;
    var timerBlink;
    function changeBackgroundImage() {
        if(i === imagesEye.length) {
            i = 0;
        }
        eyeObject[0].style.backgroundImage = imagesEye[i];
        i++;
    };

    function blinkEye (){
        setTimeout(function run () {changeBackgroundImage(); if (i<imagesEye.length){setTimeout(run,50);}}, 50);
    }

    timerBlink = setInterval(blinkEye, 5000);

})














